import { getHeroesByID } from './bases/006-destructurin';

const promesa = new Promise((resolve, reject) => {
  setTimeout(() => {
    // console.log('2 segundos');
    // resolve();

    const heroe = getHeroesByID(2);
    console.log(heroe);
    resolve(heroe);

    const heroes = getHeroesByID(3);
    reject(heroes);
  }, 2000);
});
promesa.then((heroe) => console.log(heroe)).catch((err) => console.log(err));

const getHeroesByIDAsync = (id) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const heroe = getHeroesByID(id);
      // resolve(heroe);

      if (heroe) {
        reject(heroe);
      } else {
        resolve('datos no encontrados');
      }
    }, 2000);
  });
};
// getHeroesByIDAsync(44)
//   .then((heroe) => console.log(heroe))
//   .catch((err) => console.log(err));

getHeroesByIDAsync(1).then(console.log).catch(console.error);
