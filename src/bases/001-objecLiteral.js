const persona = {
  name: 'Tonny',
  apellido: 'Stark',
  edad: 45,
  direccion: {
    ciudad: 'New York',
    lat: 589293321,
    lng: 432989778,
  },
};
// console.table({ persona });
// console.table(persona);

console.log(persona);

const persona2 = { ...persona };
persona2.name = 'maycol';

console.log(persona2);
