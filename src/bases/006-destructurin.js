import heroes from '../data/heroes';

console.log(heroes);

// const getHeroesByID = (id) => {
//   return heroes.find((heroe) => {
//     if (heroe.id === id) {
//       return true;
//     } else {
//       return false;
//     }
//   });
// };

export const getHeroesByID = (id) => {
  return heroes.find((heroe) => heroe.id === id);
};
// console.log(getHeroesByID(2));

export const getHeroesOWner = (owner) => {
  return heroes.filter((heroe) => heroe.owner === owner);
};

// console.log(getHeroesOWner('Marvel'));
