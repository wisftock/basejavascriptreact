const personaje = ['goku', 'vegeta', 'trunks'];

const [p1] = personaje;
const [, p2] = personaje;
const [, , p3] = personaje;

console.log(p1);
console.log(p2);
console.log(p3);

//

const returnArray = () => {
  return ['ASD', 123];
};

console.log(returnArray());
const [letras, numeros] = returnArray();
console.log(letras, numeros);

//

const useSate = (valor) => {
  return [
    valor,
    () => {
      console.log('Hello friend');
    },
  ];
};

const arr = useSate('codigo');
console.log(arr);

const [nombre, setNombre] = useSate('codigo');
console.log(nombre);
setNombre();
