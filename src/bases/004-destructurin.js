const person = {
  name: 'pepe',
  age: 31,
  password: 'Iroman',
  range: 'soldado',
};

// const { name, age, password } = person;

// console.log(name, age, password);

// const returnPerson = ({ name, age, password, range = 'capitan' }) => {
//   console.log(name, age, password, range);
// };
// returnPerson(person);

const useContext = ({ name, age, password, range = 'capitan' }) => {
  return {
    names: name,
    ages: age,
    latlng: {
      lat: 1234432,
      lng: -324223,
    },
  };
};
const avenger = useContext(person);
const {
  names,
  ages,
  latlng: { lat, lng },
} = avenger;
console.log(names, ages);
console.log(lat, lng);
