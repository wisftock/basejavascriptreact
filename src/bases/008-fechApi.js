const API_KEY = 'XxI7MseBbCI8DEwSRRlwvzmqPDigBTkh';

fetch(`https://api.giphy.com/v1/gifs/random?api_key=${API_KEY}`)
  .then((response) => response.json())
  .then((result) => {
    console.log(result.data.images.original.url);
    const { url } = result.data.images.original;

    const image = document.createElement('img');
    image.src = url;

    document.body.appendChild(image);
  })
  .catch((err) => console.log(err));
