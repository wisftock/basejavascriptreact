// function implicita
const getUser = () => ({
  id: 'AVS: 13421',
  username: 'Pepe',
});
console.log(getUser());

const user = getUser();
console.log(user);

/* TAREA */
// 1 transformar a una funcion implicita
const getUsuarioActivo = (nombre) => ({
  id: 'AVS 5432',
  username: nombre,
});

const usuarioActivo = getUsuarioActivo('Kike');
console.log(usuarioActivo);
