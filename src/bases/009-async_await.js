const getImage = async () => {
  try {
    const API_KEY = 'XxI7MseBbCI8DEwSRRlwvzmqPDigBTkh';
    const respt = await fetch(
      `https://api.giphy.com/v1/gifs/random?api_key=${API_KEY}`
    );
    const { data } = await respt.json();
    const { url } = await data.images.original;

    const image = document.createElement('img');
    image.src = url;

    document.body.appendChild(image);
  } catch (error) {
    console.log(err);
  }
};
getImage();
